import type { NextPage } from "next";
import { useEffect, useState } from "react";
import { usAutocompletePro } from "smartystreets-javascript-sdk";
import { client, Lookup } from "./setup-smarty";

const MAX_RESULTS = 10;

const Home: NextPage = () => {
  const [value, setValue] = useState("");
  const [suggestions, setSuggestions] = useState<
    usAutocompletePro.Suggestion[]
  >([]);

  const handleRequest = async () => {
    try {
      const lookup = new Lookup(value);
      lookup.maxResults = MAX_RESULTS;

      const response = await client.send(lookup);

      setSuggestions(response.result);
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => {
    if (value.length > 4) {
      handleRequest();
    }
  }, [value]);

  return (
    <div>
      Type here to get autocomplete suggestions:{" "}
      <input value={value} onChange={(e) => setValue(e.target.value)} />
      {suggestions.map((suggestion) => (
        <div>
          {suggestion.streetLine}, {suggestion.state}, {suggestion.city},{" "}
          {suggestion.zipcode}
        </div>
      ))}
    </div>
  );
};

export default Home;
