import SmartySDK from "smartystreets-javascript-sdk";

const key = process.env.NEXT_PUBLIC_SMARTY_EMBEDDED_KEY || "";
const licenses = ["us-autocomplete-pro-cloud"];

const SmartyCore = SmartySDK.core;

const credentials = new SmartyCore.SharedCredentials(key);
const clientBuilder = new SmartyCore.ClientBuilder(credentials).withLicenses(
  licenses
);

export const Lookup = SmartySDK.usAutocompletePro.Lookup;
export const client = clientBuilder.buildUsAutocompleteProClient();
